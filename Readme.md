# Workshop 06 - SQL con Northwind

Para la realizacion de este proyecto se necesita realzar la instalacion del Workbench proveniente de mysql, para el cual vamos a utilizar el siguiente direccion `https://dev.mysql.com/downloads/workbench/`, Una ves descargado e instaldo el programa vamos a realizar una migracion a la base de datos que el profe nos habia dado para realizar todas las sentencias

## Primer Desafio

Recupere el código (id) y la descripción (type_name) de los tipos de
movimiento de inventario (inventory_transaction_types)

![coamdo tree](img/1.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/2.jpg "Archivo de Imagen")     

## Segundo Desafio

Recupere la cantidad de ordenes (orders) registradas por cada vendedor (employees)*

![coamdo tree](img/3.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/4.jpg "Archivo de Imagen")  


## Tercer Desafio
Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.

Deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades.

![coamdo tree](img/5.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/6.jpg "Archivo de Imagen")    

## Cuarto Desafio

Recupere el monto total (invoices, orders, order_details, products) y la cantidad de
facturas (invoices) por vendedor (employee). Debe considerar solamente las ordenes
con estado diferente de 0 y solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento, no considere los
 impuestos, porque la comisión a los vendedores se paga sobre el precio base.

![coamdo tree](img/7.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/8.jpg "Archivo de Imagen")   

## Quinto Desafio

Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.
Debe agrupar por producto (inventory_transactions.product_id) y
deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades
ingresadas.

![coamdo tree](img/9.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/10.jpg "Archivo de Imagen")

## Sexto Desafio

Recupere los movimientos de inventario del tipo salida. Tomando como base todos
 los movimientos de inventario (inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.

Debe agrupar por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto
(product_name) y la cantidad de unidades que salieron.

![coamdo tree](img/11.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/12.jpg "Archivo de Imagen")   

## Septimo Desafio

Genere un reporte de movimientos de inventario (inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas fechas).

Debe incluir como mínimo el código (product_code), el nombre del producto (product_name), la fecha truncada
(transaction_created_date), la descripción del tipo de movimiento
(type name) y la suma de cantidad (quantity) .

![coamdo tree](img/13.jpg "Archivo de Imagen")     

Resultado de la sentencia

![coamdo tree](img/14.jpg "Archivo de Imagen")   

## Octavo Desafio

El la octaba sentencia no sube como llegar a realizarla lo intente pero no lo logre

