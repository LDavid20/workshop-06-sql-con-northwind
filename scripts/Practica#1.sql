/*Practica #1

Recupere el código (id) y la descripción (type_name) de los tipos de
movimiento de inventario (inventory_transaction_types)
*/

 /*Sentencia utilizada para la realizacion de la practica*/
 
 desc inventory_transaction_types;
 
 SELECT id as 'Codigo',
       type_name as 'Descripción'
     From inventory_transaction_types;