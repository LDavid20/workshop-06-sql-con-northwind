/*Practica #2

Recupere la cantidad de ordenes (orders) registradas por cada vendedor (employees)*/

 /*Sentencia utilizada para la realizacion de la practica*/
 
DESC orders;
DESC employees;

SELECT concat(e.first_name, ' ', e.last_name) as 'Vendedor',
			count(1) as 'Cantidad'
            FROM orders o
            JOIN employees e
            ON e.id = o.employee_id
            GROUP BY o.employee_id;