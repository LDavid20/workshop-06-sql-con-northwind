/*Practica #5

Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.
Debe agrupar por producto (inventory_transactions.product_id) y
deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades
ingresadas.
*/

 /*Sentencia utilizada para la realizacion de la practica*/

select p.product_code as 'Codigo', p.product_name as 'Producto', it.quantity as 'Cantidad'
from northwind.inventory_transactions it join northwind.products p on it.product_id = p.id
where it.transaction_type = 1 group by it.product_id