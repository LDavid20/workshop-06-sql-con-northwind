/*Practica #7

Genere un reporte de movimientos de inventario (inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas fechas).

Debe incluir como mínimo el código (product_code), el nombre del producto (product_name), la fecha truncada
(transaction_created_date), la descripción del tipo de movimiento
(type name) y la suma de cantidad (quantity) .
*/

 /*Sentencia utilizada para la realizacion de la practica*/
 
SELECT product_code as 'codigo', product_name as 'producto', type_name as 'tipo', transaction_created_date as fecha, sum(quantity) as cantidad 
FROM northwind.products p JOIN northwind.inventory_transactions i
 ON p.id = i.product_id JOIN northwind.inventory_transaction_types it
 ON i.transaction_type = it.id WHERE transaction_created_date BETWEEN '2006/03/22' AND '2006/03/24'
GROUP BY p.product_name, i.transaction_type;