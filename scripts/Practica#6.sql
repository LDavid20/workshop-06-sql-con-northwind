/*Practica #6

Recupere los movimientos de inventario del tipo salida. Tomando como base todos
 los movimientos de inventario (inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.

Debe agrupar por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto
(product_name) y la cantidad de unidades que salieron.
*/

select p.product_code as 'Codigo', p.product_name as 'Producto', sum(it.quantity) as 'Cantidad'
from northwind.inventory_transactions it join northwind.products p on it.product_id = p.id
where it.transaction_type in (2,3,4) group by it.product_id